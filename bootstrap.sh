#!/usr/bin/env bash

apt-get update
wget https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.14.2.linux-amd64.tar.gz
echo "PATH=\$PATH:/usr/local/go/bin" >> /home/vagrant/.profile

wget https://nodejs.org/dist/v12.16.3/node-v12.16.3-linux-x64.tar.xz
tar -C /usr/local -xf node-v12.16.3-linux-x64.tar.xz
mv /usr/local/node-v12.16.3-linux-x64 /usr/local/node
echo "PATH=\$PATH:/usr/local/node/bin" >> /home/vagrant/.profile

export PATH=$PATH:/usr/local/node/bin

npm install -g yarn

rm go1.14.2.linux-amd64.tar.gz
rm node-v12.16.3-linux-x64.tar.xz